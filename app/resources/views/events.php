<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>WebEx Web App - Events</title>

    <!-- Bootstrap core CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <!-- Custom styles for this events -->
    <link href="/css/style.css" rel="stylesheet">
  </head>

  <body class="text-center">
      <main role="main" class="container">
      <div class="my-3 p-3 bg-white rounded box-shadow">

        <h6 class="border-bottom border-gray pb-2 mb-0">Scheduled Meeting List</h6>

      <?php if (isset($errMsg)) :?>
        <div class="alert alert-warning" role="alert">
          <?php print @$errMsg; ?>
        </div>
      <?php endif;?>

        <table class="table table-hover text-left">
          <thead>
            <tr>
              <th scope="col">SessionKey</th>
              <th scope="col">SessionName</th>
              <th scope="col">SessionType</th>
              <th scope="col">WebExId</th>
              <th scope="col">StartDate</th>
              <th scope="col">EndDate</th>
              <th scope="col">Status</th>
              <th scope="col">Actions</th>
            </tr>
          </thead>
          <tbody>
            <?php if ($eventList):?>
              <?php foreach($eventList as $k => $event):?>
               <tr>
                  <th><?php print $event['event:sessionKey'];?></th>
                  <td><?php print $event['event:sessionName'];?></td>
                  <td><?php print $event['event:sessionType'];?></td>
                  <td><?php print $event['event:hostWebExID'];?></td>
                  <td><?php print $event['event:startDate'];?></td>
                  <td><?php print $event['event:endDate'];?></td>
                  <td><?php print $event['event:status'];?></td>
                  <td><a href="/delete/<?php print $event['event:sessionKey'];?>">delete</a></td>
                </tr>
              <?php endforeach;?>
            <?php endif;?>
          </tbody>
        </table>

        <small class="d-block text-right mt-3">
          <a href="/create">Create Random Event</a> | <a href="/logout">Logout</a>
        </small>

      </div>
    </main>
  </body>
</html>
