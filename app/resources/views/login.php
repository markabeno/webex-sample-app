<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>WebEx Web App Login</title>

    <!-- Bootstrap core CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <!-- Custom styles for this template -->
    <link href="/css/style.css" rel="stylesheet">
  </head>

  <body class="text-center">
    <form class="form-signin" action="/login" method="post">
      <img class="mb-4" src="http://unclineberger.org/unccn/about-unccn/images/webex/image_preview" alt="" width="72" height="72">

      <h1 class="h3 mb-3 font-weight-normal">Please sign in</h1>

      <?php if (isset($errMsg)) :?>
        <div class="alert alert-warning" role="alert">
          <?php print @$errMsg; ?>
        </div>
      <?php endif;?>

      <label for="inputUsername" class="sr-only">WebEx Username</label>
      <input type="" id="inputUsername" name=username class="form-control username" placeholder="WebEx Username" required autofocus>

      <label for="inputPassword" class="sr-only">WebEx Password</label>
      <input type="password" id="inputPassword" name="password" class="form-control" placeholder="WebEx Password" required>

      <label for="inputSiteId" class="sr-only">WebEx SiteId</label>
      <input type="" id="inputSiteId" name="siteid" class="form-control siteid" placeholder="WebEx SiteId" required autofocus>

      <button class="btn btn-lg btn-primary btn-block" type="submit">Connect</button>
    </form>
  </body>
</html>
