<?php

namespace App\Http\Controllers;
use App\Services\WebExService;
use Illuminate\Http\Request;
use Illuminate\Http\Response;


class LoginController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
        Handles landing page view
    */
    public function index(Request $request) {
        return view('login', ['errMsg' => $request->session()->get('errorMsg') ]);
    }

    /**
        Handles authentication
    */
    public function login(Request $request) {

        $username = $request->input('username');
        $password = $request->input('password');
        $siteid = $request->input('siteid');

        $webex = new WebExService();
        $webex->set_auth($username, $password, $siteid);
        $webex->set_url("https://$siteid.webex.com");
        $response = $webex->user_AuthenticateUser();
        if (isset($response['serv:header']['serv:response']['serv:result']) &&
            $response['serv:header']['serv:response']['serv:result'] === 'SUCCESS'
        ) {
            $sessionToken = $response['serv:body']['serv:bodyContent']['use:sessionTicket'];
            $request->session()->put('token', $sessionToken);

            // Duh, this is a bad practise, but due to limited time and no documentation on
            // how to handle authenticated requests, let's do this magic for now.
            $request->session()->put('username', $username);
            $request->session()->put('password', $password);
            $request->session()->put('siteid', $siteid);

            return redirect('/events');
        } else{
            $error = $response['serv:header']['serv:response']['serv:subErrors']['serv:subError']['serv:reason'];
            $request->session()->flash('errorMsg', ucwords($error));
            return redirect('/login');
        }
    }

    public function logout(Request $request) {
        $request->session()->flush();
        return redirect('/login');
    }
}
