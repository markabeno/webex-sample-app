<?php

namespace App\Http\Controllers;
use App\Services\WebExService;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class EventController extends Controller
{

    protected $username;
    protected $password;
    protected $siteid;
    protected $webex;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $request)
    {
        $this->username = $request->session()->get('username');
        $this->password = $request->session()->get('password');
        $this->siteid = $request->session()->get('siteid');
        $this->webex = new WebExService();
        $this->webex->set_auth($this->username, $this->password, $this->siteid);
        $this->webex->set_url("https://$this->siteid.webex.com");
    }

    /**
    * Checks if user creds are set
    */
    public function has_auth() {
        return (bool) $this->username && $this->password && $this->siteid;
    }

    /**
    * Events landing page
    * @param object $request
    */
    public function index(Request $request) {

        if (!$this->has_auth()){
            return redirect('/login');
        }
        $response = $this->webex->event_LstsummaryEvent();
        if (isset($response['serv:header']['serv:response']['serv:result']) &&
            $response['serv:header']['serv:response']['serv:result'] === 'SUCCESS'
        ) {
            $records = $response['serv:body']['serv:bodyContent']['event:matchingRecords']['serv:total'];
            $eventList = $response['serv:body']['serv:bodyContent']['event:event'];
            if ($records == 1) {
                $eventList = [$eventList];
            }
        } else {
            $eventList = [];
            $errorMsg = $response['serv:header']['serv:response']['serv:reason'];
            $request->session()->flash('errorMsg', ucwords($errorMsg));
        }
        return view('events', ['eventList' => $eventList ]);
    }

    /**
    * Creates an event with random event name.
    * @param object $request
    * @return void
    */
    public function create(Request $request) {

        if (!$this->has_auth()){
            return redirect('/login');
        }

        $eventName = 'WebEx_'.$this->quickRandom(6);
        $eventDate = date('m/d/Y h:i:s', strtotime("+1 day"));
        $response = $this->webex->event_CreateEvent($eventName, $eventDate);
        if (isset($response['serv:header']['serv:response']['serv:result']) &&
            $response['serv:header']['serv:response']['serv:result'] === 'SUCCESS'
        ) {
            $sessionKey = $response['serv:body']['serv:bodyContent']['event:sessionKey'];
            return redirect('/events');
        } else{
            $error = $response['serv:header']['serv:response']['serv:subErrors']['serv:subError']['serv:reason'];
            $request->session()->flash('errorMsg', ucwords($error));
            return redirect('/events');
        }
    }

    /**
    * Removes/Deletes event in webex
    * @param string $sessionkey Session key of an event from webex
    * @return void
    */
    public function delete($sessionkey) {
        if (!$this->has_auth()){
            return redirect('/login');
        }
        $response = $this->webex->event_DelEvent($sessionkey);
        return redirect('/events');
    }

    /**
     * Generate a "random" alpha-numeric string.
     * Should not be considered sufficient for cryptography, etc.
     * @param  int  $length
     * @return string
     */
    public static function quickRandom($length = 16)
    {
        $pool = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';

        return substr(str_shuffle(str_repeat($pool, $length)), 0, $length);
    }
}
