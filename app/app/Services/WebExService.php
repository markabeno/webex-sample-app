<?php
namespace App\Services;
use Nathanmac\Utilities\Parser\Parser;

class WebExService
{
    private $username;
    private $password;
    private $siteID;
    private $url_prefix;
    private $url_host;
    private $send_mode;
    private $data;
    private $action;

    const SEND_CURL             = 'curl';
    const SEND_FSOCKS           = 'fsocks';
    const PREFIX_HTTP           = 'http';
    const PREFIX_HTTPS          = 'https';
    const SUFIX_XML_API         = 'WBXService/XMLService';
    const WEBEX_DOMAIN          = 'webex.com';
    const XML_VERSION           = '1.0';
    const XML_ENCODING          = 'UTF-8';
    const USER_AGENT            = 'WebEx App';

    const API_SCHEMA_MEETING    = 'http://www.webex.com/schemas/2002/06/service/meeting';
    const API_SCHEMA_EVENT      = 'http://www.webex.com/schemas/2002/06/service/event';
    const API_SCHEMA_AUTHUSER   = 'http://www.webex.com/schemas/2002/06/service/user';
    const API_SCHEMA_SERVICE    = 'http://www.webex.com/schemas/2002/06/service';

    const DATA_SENDER               = 'sender';
    const DATA_SENDER_POST_HEADER   = 'post_header';
    const DATA_SENDER_POST_BODY     = 'post_body';
    const DATA_SENDER_XML           = 'xml';
    const DATA_RESPONSE             = 'response';
    const DATA_RESPONSE_XML         = 'xml';
    const DATA_RESPONSE_DATA        = 'data';

    public function __construct()
    {
        $this->action = 0;
        $this->response = array();
        $this->send_mode = in_array(self::SEND_CURL, get_loaded_extensions()) ? self::SEND_CURL : self::SEND_FSOCKS;
    }

    /**
     * Test if have a auth data to use a API.
     *
     * @return bool Returns true if have data and false if not.
     */
    public function has_auth() {
        return (bool) $this->username && $this->password && $this->siteID;
    }

    /**
     * Auth data to integrate with API.
     *
     * @param string $username Username to auth.
     * @param string $password Password to auth.
     * @param string $siteID Customer site id.
     * @param string $partnerID Customer partnerID id.
     * @return void
     */
    public function set_auth($username, $password, $siteID, $partnerID = '') {
        $this->username     = $username;
        $this->password     = $password;
        $this->siteID       = $siteID;
        $this->partnerID    = $partnerID;
    }

    /**
     * Set a url to integrate to webex.
     *
     * @param string $url Customer url.
     * @return void
     */
    public function set_url($url) {
        if(!self::validate_url($url))
            exit(__CLASS__ . ' error report: Wrong webex url');
        list($this->url_prefix, $this->url_host) = preg_split("$://$", $url);
    }

    /**
     * Validates a customer webex domain.
     *
     * @param string $url Url to validate.
     * @return bool Return true if a valid url or false if not.
     */
    public static function validate_url($url) {
        $regex = "/^(http|https):\/\/(([A-Z0-9][A-Z0-9_-]*)+.(" . self::WEBEX_DOMAIN . ")$)/i";
        return (bool) preg_match($regex, $url);
    }

    /**
     * Generates a XML to send a data to API.
     *
     * @param array $data Data to insert in XML in format:
     *      $data['service']    = 'meeting';
     *      $data['xml_header'] = '<item><subitem>data</subitem></item>';
     *      $data['xml_body']   = '<item><subitem>data</subitem></item>';
     * @return string Returns a XML generated.
     */
    private function get_xml($data) {
        $xml    = array();
        $xml[]  = '<?xml version="' . self::XML_VERSION . '" encoding="' . self::XML_ENCODING . '"?>';
        $xml[]  =   '<serv:message xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">';
        $xml[]  =       '<header>';
        $xml[]  =           '<securityContext>';
        $xml[]  =               '<webExID>' . $this->username . '</webExID>';
        $xml[]  =               '<password>' . $this->password . '</password>';
        $xml[]  =               '<siteName>' . $this->siteID . '</siteName>';
        if(isset($data['xml_header']))
            $xml[]  =           $data['xml_header'];
        $xml[]  =           '</securityContext>';
        $xml[]  =       '</header>';
        $xml[]  =       '<body>';
        $xml[]  =           '<bodyContent xsi:type="java:com.webex.service.binding.' . $data['service'] . '">';
        $xml[]  =               $data['xml_body'];
        $xml[]  =           '</bodyContent>';
        $xml[]  =       '</body>';
        $xml[]  =   '</serv:message>';
        return implode('', $xml);
    }

    /**
     * Generates a header and a body to send data to API.
     *
     * @return string Returns a response from API.
     */
    private function send($xml) {
        $post_data['UID'] = $this->username;
        $post_data['PWD'] = $this->password;
        $post_data['SID'] = $this->siteID;
        $post_data['XML'] = $xml;

        // Really I dont know why xml api give a error on http_build_query :(
        $post_string = '';
        foreach ($post_data as $variable => $value) {
            $post_string .= '' . $variable . '=' . urlencode($value) . '&';
        }

        $post_header        = array();
        $post_header[]      = 'POST /' . self::SUFIX_XML_API . ' HTTP/1.0';
        $post_header[]      = 'Host: ' . $this->url_host;
        $post_header[]      = 'User-Agent: ' . self::USER_AGENT;
        if($this->send_mode == self::SEND_FSOCKS) {
            $post_header[]  = 'Content-Type: application/xml';
            $post_header[]  = 'Content-Length: ' . strlen($xml);
        }
        $data = array();
        $data['post_header'] = $post_header;
        $data['post_string'] = $post_string;

        $this->data[$this->action][self::DATA_SENDER][self::DATA_SENDER_POST_HEADER]    = $post_header;
        $this->data[$this->action][self::DATA_SENDER][self::DATA_SENDER_POST_BODY]      = $post_header;
        $this->data[$this->action][self::DATA_SENDER][self::DATA_SENDER_XML]            = $xml;

        return $this->{'send_' . $this->send_mode}($data);
    }

    /**
     * Get port used by API.
     *
     * @param string $prefix Prefix to get a port.
     * @return int Return a port.
     */
    public static function get_port($prefix) {
        switch($prefix) {
            case self::PREFIX_HTTP:
                return 80;
            break;
            case self::PREFIX_HTTPS:
                return 443;
            break;
            default:
                exit(__CLASS__ . ' error report: Wrong prefix');
            break;
        }
    }

    /**
     * Send a data to Webex API using PHP curl.
     *
     * @param array $data Data to send to API in format:
     *      $data['post_header'] = "blablabla";
     *      $data['post_header'] = "post_string";
     * @return string Returns a response from API.
     */
    private function send_curl($data) {
        extract($data);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->url_prefix . '://' . $this->url_host . '/' . self::SUFIX_XML_API);
        curl_setopt($ch, CURLOPT_PORT, self::get_port($this->url_prefix));
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $post_header);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $response = curl_exec($ch);
        if($response === false)
            exit(__CLASS__ . ' error report: Curl error - ' . curl_error($ch));
        curl_close($ch);
        return $response;
    }

    public function user_AuthenticateUser() {
        if(!$this->has_auth())
            exit(__CLASS__ . ' error report: Auth data not found');

        $xml_body   = array();
        $xml_body[] = '';
        $data['xml_body']   = implode('', $xml_body);
        $data['service']    = 'user.AuthenticateUser';
        $xml                = $this->get_xml($data);
        $response           = $this->send($xml);
        $parser = new Parser();
        return $parser->xml($response);
    }

    /**
    * Creates an event in webex
    * @param string $eventName
    * @param string $eventDate
    * @return array
    */
    public function event_CreateEvent($eventName, $eventDate) {
        if(!$this->has_auth())
            exit(__CLASS__ . ' error report: Auth data not found');

        $data['xml_body']   =  "
            <accessControl>
                <listing>PUBLIC</listing>
                <sessionPassword>111111</sessionPassword>
            </accessControl>
            <schedule>
                <startDate>$eventDate</startDate>
                <duration>5</duration>
                <timeZoneID>45</timeZoneID>
                <entryExitTone>NOTONE</entryExitTone>
            </schedule>
            <enrollment>
                <endURLAfterEnroll>www.yahoo.com</endURLAfterEnroll>
                <idReq>true</idReq>
                <passwordReq>true</passwordReq>
                <password>password</password>
                <approvalReq>true</approvalReq>
                <approvalRules>
                    <rule>
                        <enrollFieldID>-4</enrollFieldID>
                        <condition>CONTAINS</condition>
                        <queryField>query word</queryField>
                        <action>REJECT</action>
                        <matchCase>false</matchCase>
                    </rule>
                </approvalRules>
            </enrollment>
            <metaData>
                <sessionName>$eventName</sessionName>
                <description>This is a test event</description>
            </metaData>
            <tracking>
                <trackingCode1>test1</trackingCode1>
                <trackingCode2>test2</trackingCode2>
                <trackingCode3>test3</trackingCode3>
                <trackingCode4>test4</trackingCode4>
                <trackingCode5>test5</trackingCode5>
                <trackingCode6>test6</trackingCode6>
                <trackingCode7>test7</trackingCode7>
                <trackingCode8>test8</trackingCode8>
                <trackingCode9>test9</trackingCode9>
                <trackingCode10>test10</trackingCode10>
            </tracking>
            <attendees>
                <attendee>
                    <name>attendee01</name>
                    <title>attendeeTitle</title>
                    <company>webex</company>
                    <webExId>test</webExId>
                    <address>
                        <addressType>PERSONAL</addressType>
                        <address1>String1</address1>
                        <address2>String2</address2>
                        <city>String1</city>
                        <state>String2</state>
                        <zipCode>215011</zipCode>
                        <country>china</country>
                    </address>
                    <phones>
                        <phone>456566</phone>
                        <mobilePhone>9076</mobilePhone>
                        <fax>46</fax>
                    </phones>
                    <email>jenny.solmarf@gmail.com</email>
                    <notes>String</notes>
                    <url>String</url>
                    <type>VISITOR</type>
                </attendee>
            </attendees>
            <extOptions>
                <enrollmentNumber>100</enrollmentNumber>
                <destinationURL>String</destinationURL>
                <allowInviteFriend>true</allowInviteFriend>
                <viewAttendeeList>HOST,PRESENTER,PANELISTS</viewAttendeeList>
            </extOptions>
            <emailTemplates>
                <format>TEXT</format>
                <invitationMsgs>
                    <participantsEmail>
                        <subject>Your invitation to -- %Topic%</subject>
                        <from>%SenderEmailAddress%</from>
                        <replyTo>%HostEmail%</replyTo>
                        <content>Hello %ParticipantName%, %HostName% has invited you
                            to attend a Webinar on the web using WebEx. Topic: %Topic%
                            Date: %MeetingDate% Time: %MeetingTime%, %EventTimeZone%
                            Enrollment password: %RegistrationPassword% To attend
                            this webinar, you must first register for it. Please click
                            the following link to see more information about and
                            register for this event. Once you have registered for the
                            session, you will receive an email message confirming your
                            registration. This message will provide the information
                            that you need to join the session. Please click the
                            following link to see more information about the event and
                            register. %MeetingInfoURL% To contact %HostName%,
                            %PhoneContactInfo% send a message to this address:
                            %HostEmail% %EmailFooter%
                        </content>
                    </participantsEmail>
                    <panelistsEmail>
                        <subject>
                            You're invited to be a panelist a Webinar: -- %Topic%
                        </subject>
                        <from>%SenderEmailAddress%</from>
                        <replyTo>%HostEmail%</replyTo>
                        <content>Hello %PanelistName%, %HostName% has invited you to
                            be a panelist in a Webinar on the web using WebEx. Topic:
                            %Topic% Date: %MeetingDate% Time: %MeetingTime%,
                            %EventTimeZone% Event Number: %MeetingNumber% Event
                            Entrance for Attendees: %MeetingInfoURL% Panelist
                            Password: %PanelistPassword% (Please do not share
                            panelist password) Teleconference: %TeleconferenceInfo%
                            %PanelistJoinBeforeHost% %PanelistEntranceURL%
                            %UCFAttendeeVerifyPlayers% To contact %HostName%,
                            %PhoneContactInfo% send a message to this address:
                            %HostEmail% %EmailFooter%
                        </content>
                    </panelistsEmail>
                </invitationMsgs>
                <enrollmentMsgs>
                    <pendingEmail>
                        <subject>Enrollment Pending</subject>
                        <from>%SenderEmailAddress%</from>
                        <replyTo>%HostEmail%</replyTo>
                        <content>Hello %AttendeeName%, Your registration for this
                            event is now pending. Topic: %Topic% Date: %MeetingDate%
                            Time: %MeetingTime%, %EventTimeZone% We will send an
                            updated email to you once your status changes. To contact
                            %HostName%, call %HostPhone% send a message to this
                            address: %HostEmail% %EmailFooter%
                        </content>
                        <send>true</send>
                    </pendingEmail>
                </enrollmentMsgs>
            </emailTemplates>
            <enrollmentForm>
                <standardFields>
                    <city>
                        <incl>true</incl>
                        <req>false</req>
                    </city>
                    <state>
                        <incl>true</incl>
                        <req>false</req>
                    </state>
                </standardFields>
                <customFields>
                    <textBox>
                        <incl>true</incl>
                        <req>true</req>
                        <label>old</label>
                        <type>SINGLE_LINE</type>
                        <width>50</width>
                    </textBox>
                    <checkBoxGroup>
                        <incl>true</incl>
                        <req>false</req>
                        <label>sex</label>
                        <checkBox>
                            <label>man</label>
                            <score>1</score>
                            <state>SELECTED</state>
                        </checkBox>
                        <checkBox>
                            <label>woman</label>
                            <score>2</score>
                            <state>CLEARED</state>
                        </checkBox>
                    </checkBoxGroup>
                </customFields>
            </enrollmentForm>
        ";
        $data['service']    = 'event.CreateEvent';
        $xml                = $this->get_xml($data);
        $response           = $this->send($xml);
        $parser = new Parser();
        return $parser->xml($response);
    }

    public function event_LstsummaryEvent() {
        if(!$this->has_auth())
            exit(__CLASS__ . ' error report: Auth data not found');

        $data['xml_body']   = "
            <listControl>
                <startFrom>1</startFrom>
                <maximumNum>10</maximumNum>
                <listMethod>OR</listMethod>
            </listControl>
            <order>
                <orderBy>HOSTWEBEXID</orderBy>
                <orderAD>ASC</orderAD>
                <orderBy>EVENTNAME</orderBy>
                <orderAD>ASC</orderAD>
                <orderBy>STARTTIME</orderBy>
                <orderAD>ASC</orderAD>
            </order>
            <dateScope>
                <startDateStart>03/10/2004 00:00:00</startDateStart>
                <timeZoneID>45</timeZoneID>
            </dateScope>
        ";
        $data['service']    = 'event.LstsummaryEvent';
        $xml                = $this->get_xml($data);
        $response           = $this->send($xml);
        $parser = new Parser();
        return $parser->xml($response);
    }

    public function event_DelEvent($sessionKey) {
        if(!$this->has_auth())
            exit(__CLASS__ . ' error report: Auth data not found');

        $data['xml_body'] = "
            <sessionKey>$sessionKey</sessionKey>
        ";
        $data['service']    = 'event.DelEvent';
        $xml                = $this->get_xml($data);
        $response           = $this->send($xml);
        $parser = new Parser();
        return $parser->xml($response);
    }
}
