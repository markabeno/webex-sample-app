# Simple WebEx App 
#### By: Mark Abeno

## Created Files
- app/app/services/webexservice.php - Class responsible for webex api connections
- app/app/http/controllers/logincontroller.php
- app/app/http/controllers/eventcontroller.php
- app/resources/views/events.php
- app/resources/views/login.php

## Functionalities
- WebEx XML API Authenctication 
- XML API Create Event
- XML API Delete Event
- XML API List All Events

## How to run this app locally
- Clone this repository
- In project root directory, run `php -S localhost:80 -t public`

## Demo Site
- http://139.162.28.193/
- WebEx Username: solmarf
- WebEx Password: JGHdv14hGsdhs  
- WebEx SideId: apidemoeu
